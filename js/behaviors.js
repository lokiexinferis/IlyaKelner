
/* Marks the current active element from a navLink */
function ToggleActive(object){
	$(".navLink").removeClass("active"); // remove the class from the currently selected
	$(object).addClass("active"); // add the class to the newly clicked link
};
	
/* Smooth page scrolling */
$(function() {
  $('a[href*=#]:not([href=#])').click(function() {
    if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
      var target = $(this.hash);
      target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
      if (target.length) {
        $('html,body').animate({
          scrollTop: target.offset().top
        }, 500);
        return false;
      }
    }
  });
});

$(function() {
	var AnimateInterval = 0;
	var sidebar = $("#navigation");
	var top = sidebar.offset().top;
	var height = sidebar.height();
	var winHeight = $(window).height();
	var gap = 0;
	
	$(window).scroll(function(event) 
	{		
		
		var scrollTop = $(this).scrollTop();
	
		if($(window).scrollTop() <= $(".ContentPanel").height() - winHeight)
		{
		// sidebar reached the (end - viewport height)
			if (scrollTop + winHeight >= top + height + gap) {
				if(AnimateInterval >= 1)
				{
					sidebar.stop().animate({
						top: scrollTop + winHeight - height
					}, AnimateInterval);
				}
				else
				{
					sidebar.css("top", scrollTop + winHeight - height);
				}
			} else {
				// otherwise remove it
				if(AnimateInterval >= 1)
				{
					sidebar.stop().animate({
						top: 0
					}, AnimateInterval);
				}
				else
				{
					sidebar.css("top", 0);
				}
			}
		}
	});
});

$(function() {
	/**
	 * This part handles the highlighting functionality.
	 * We use the scroll functionality again, some array creation and 
	 * manipulation, class adding and class removing, and conditional testing
	 */
	var aChildren = $(".navCell").children(); // find the a children of the list items
	var aArray = []; // create the empty aArray
	for (var i=0; i < aChildren.length; i++) {    
		var aChild = aChildren[i];
		var ahref = $(aChild).attr('href');
		if(ahref != undefined)
		{
			aArray.push(ahref);
		}
	} // this for loop fills the aArray with attribute href values
	
	var activated = false;
	
	$(window).scroll(function(event){
			
		var windowPos = Math.floor($(window).scrollTop()); // get the offset of the window from the top of page
		var windowHeight = Math.floor($(window).height()); // get the height of the window
		var docHeight = Math.floor($(".ContentPanel").height());
	
		for (var i=0; i < aArray.length; i++) {
			var theID = aArray[i];
			var divPos = Math.floor($(theID).offset().top); // get the offset of the div from the top of page
			var divHeight = Math.floor($(theID).height()); // get the height of the div in question
			
			var gap = 100;
			if(i > 0)
			{
				var prevID = aArray[i-1];
				var prevDivBottom = $(prevID).offset().top + $(prevID).height();
				gap = divPos - prevDivBottom;
			}
			
			if (((windowPos + windowHeight) <= docHeight))
			{
				console.log(i + ": " + windowPos + ", " + divPos + ", " + divHeight);
				if (windowPos >= divPos-gap && windowPos < (divPos + divHeight)) 
				{
					$("a[href='" + theID + "'] .navLink").addClass("active");
					activated = true;
				} 
				else 
				{
					$("a[href='" + theID + "'] .navLink").removeClass("active");
				}
			}
		}
/*		
		if(activated == false)
		{
			// Default to the 0th element
			$("a[href='" + aArray[0] + "'] .navLink").addClass("active");
		}
	*/	
		
	});
	
	if(activated == false)
	{
		// Default to the 0th element
		$("a[href='" + aArray[0] + "'] .navLink").addClass("active");
	}
});